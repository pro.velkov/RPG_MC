/**
 * l'objet caractéristique
 * @author imad
 *
 */
public class Ability {
	
	private String[] description;
	private String name;
	private int level;
	private int type;
	
	/**
	 * Ability constructor
	 * @param name le nom de la caractéristique
	 * @param description description de la caractéristique
	 * @param level le niveau de la caractéristique
	 * @param type le type de la caractéristique
	 */
	public Ability(String name, String[] description, int level, int type) {
		this.name = name;
		this.description = description;
		this.level = level;
		this.type = type;
	}
	/**
	 * Enregistre les informations des caractéristiques
	 * @return un string des options de la cractéristiques
	 */
	public String save() {
		String data ="";
		data+= this.name+",";
		data+=this.level+",";
		data+=this.type+",";
		return data;
	}
	
	/**
	 * renvoie une chaîne contenant le nom
	 * @return le nom
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * retourne un int représentant le type de capacité
	 * @return type de capacité
	 */
	public int getType() {
		return type;
	}
	
	/**
	 * renvoie une chaîne contenant la description
	 * @return la description
	 */
	public String[] getDescription(){
		return description;
	}
	
	/**
	 * sets the description to the passed String array
	 * @param description description de la caractéristique
	 */
	public void updateDescription(String[] description) {
		this.description = description;
	}
	
}
