import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
/**
 * le laucher
 * @author imad
 *
 */
public class GameMain{
	/**
	 * la m�thode main
	 * @param args l'argument main
	 */
 public static void main(String[] args){
	 
	/**
	 *  Titre de la fen�tre
	 */
	JFrame frame = new JFrame("RPGChess");
	//taskbar Icon
	ImageIcon img = new ImageIcon("images//Lord.PNG");
	frame.setIconImage(img.getImage());
	
	//JFrame stuff
	Container c = frame.getContentPane();
	GamePanel p = new GamePanel();
	c.add(p);
	frame.pack();
	// Plein ecran si besoin
	//frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
	frame.setVisible(true);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

 	}
}