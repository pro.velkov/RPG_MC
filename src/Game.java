import java.awt.event.MouseEvent;
import java.lang.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
/**
 * la classe principale pour les m�thodes utils�s dans le jeu
 * @author imad
 *
 */
public class Game {
	// variables du jeu
	public boolean gameBegun = false;
	public boolean gameOver = false;

	private ArrayList<String> moves;
	public GamePiece[] board;
	public GamePiece[] list;
	public GamePiece[] characters;
	public GamePiece[] loadedPieces;

	GamePiece markedPiece = null;
	private int redMax = 0;
	private int blueMax = 0;
	private int redDmg = 0;
	private int blueDmg = 0;
	private String winner;
	private int turn;

	/**
	 *  initialisation le jeu et tentative de chargement du jeu
	 */
	public Game() {
		// initialisation le jeu
		moves = new ArrayList<String>(1);
		board = new GamePiece[30];
		turn = 0;
		// tentative de chargement du jeu
		try {
			importPieces(board);
		} catch (FileNotFoundException e) {
			// default
			e.printStackTrace();
		}
	}
	
	/**
	 * cette m�thode permet de jouer un son
	 * @param soundName la route vers le son
	 */
	public static void playSound(String soundName) {
		try {
			AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(soundName).getAbsoluteFile());
			Clip clip = AudioSystem.getClip();
			clip.open(audioInputStream);
			clip.start();

		} catch (Exception ex) {
			System.out.println("Error with playing sound.");
			ex.printStackTrace();
		}
	}

	/**
	 *   v�rifie les conditions de mise � jour pour progresser vers la fin de la partie
	 */
	public void update() {
		if (blueCurTotalHealth() <= 0) {
			endGame();
			winner = "Red";
		} else if (redCurTotalHealth() <= 0) {
			endGame();
			winner = "Blue";
		}
		if (turn == 0) {
			turn = 1;
		} else {
			turn = 0;
		}
	}
	/**
	 * Qui va Jouer?
	 * @param turn mettre le tour � l'�quipe oppos�e
	 */
	public void setTurn(int turn) {
		this.turn = turn;
	}


	/**
	 *  renvoie les mouvements les plus r�cents
	 * @return les mouvement effectu�s
	 */
	public ArrayList<String> getMoves() {
		return moves;
	}

	/**
	 *  mouvement des pieces
	 * @param x posistion x en pixel
	 * @param y posistion y en pixel
	 * @param piece la piece qu'on veut bouger
	 * @param type le type 0 est le d�placement, le type 1 est l'attaque
	 */
	public void updateMoves(int x, int y, GamePiece piece, int type) {
		// move
		if (type == 0) {
			moves.add(piece.getTeam() + " moved " + piece.getName() + " from: " + Integer.toString(x) + ", "
					+ Integer.toString(y) + " to: " + Integer.toString(piece.getX()) + ", "
					+ Integer.toString(piece.getY()));
		}
		// d�placement incorrect
		else if (type == 1) {
			moves.add("Invalid move");
		}
		// attaque
		else {
			moves.add(piece.getTeam() + " attacked " + getPiece(x, y).getName() + " with " + piece.getName() + " for "
					+ piece.getDamage() + " damage");
		}
		if (moves.size() > 20) {
			moves.remove(0);
		}
	}

	/**
	 *  renvoie le gagnant du jeu sous la forme d'une cha�ne
	 * @return la couleur du gagnant
	 */
	public String getWinner() {
		return winner;
	}

	/**
	 *  commence le jeu
	 */
	public void beginGame() {
		gameBegun = true;

	}

	/**
	 *  red�marre le jeu
	 */
	public void restart() {
		gameBegun = false;
		gameOver = false;
		board = new GamePiece[30];
		// attempt to load the game
		markedPiece = null;
		redMax = 0;
		blueMax = 0;
		redDmg = 0;
		blueDmg = 0;
		turn = 0;
		winner = null;
		moves.clear();
		try {
			importPieces(board);
		} catch (FileNotFoundException e) {
			// default
			e.printStackTrace();
		}
	}

	/**
	 * retourne vrai si le jeu a commenc�, faux sinon
	 * @return le jeu commance
	 */
	public boolean isBegun() {
		return gameBegun;
	}
	/**
	 * Retourne au menu principale
	 */
	public void isBack() {
		gameBegun = false;
		restart();
	}


	/**
	 * met fin � la partie en cours
	 */
	public void endGame() {
		gameOver = true;
	}

	/**
	 * retourne vrai si le jeu est termin�, faux sinon
	 * @return le jeu et fini
	 */
	public boolean isOver() {
		return gameOver;
	}

	/**
	 * renvoie l'int du total des dommages que tous les personnages bleus ont inflig� pendant la partie
	 * @return damage bleu
	 */
	public int blueDamageDealt() {
		return blueDmg;
	}

	/**
	 * renvoie un int du total des dommages que tous les personnages rouges ont inflig�s pendant la partie.
	 * @return damage rouge
	 */
	public int redDamageDealt() {
		return redDmg;
	}

	/**
	 * renvoie un int du total de la sant� restante de tous les personnages bleus
	 * @return sant� bleu
	 */
	public int blueCurTotalHealth() {
		int sum = 0;
		for (int i = 0; i < 10; i++) {
			if (board[i] != null && board[i].isAlive()&&board[i].getTeamAsInt()==0) {
				sum += board[i].getHealth();
			}
		}
		return sum;
	}
	
	/**
	 * renvoie un nombre entier de la sant� maximale totale de tous les personnages bleus.
	 * @return points vie bleu
	 */
	public int blueMaxTotalHealth() {
		if (blueMax != 0) {
			return blueMax;
		}
		int sum = 0;
		for (int i = 0; i < 10; i++) {
			if(this.board[i]!=null&&board[i].getTeamAsInt()==0)
			sum += board[i].getMaxHealth();
		}
		blueMax = sum;
		return sum;
	}

	/**
	 * renvoie le nombre total de points de vie restants pour tous les personnages rouges.
	 * @return point vie rouge
	 */
	public int redCurTotalHealth() {
		int sum = 0;
		for (int i = 1; i < 30; i++) {
			if (board[i] != null &&board[i].getTeamAsInt()==1) {
				sum += board[i].getHealth();
			}
		}
		return sum;
	}

	/**
	 * renvoie un nombre entier de la sant� maximale totale de tous les personnages rouges.
	 * @return sant� rouge
	 */
	public int redMaxTotalHealth() {
		
		if (redMax != 0) {
			return redMax;
		}
		int sum = 0;
		for (int i = 1; i < 30; i++) {
			if(board[i]!=null&&board[i].getTeamAsInt()==1) {
			sum += board[i].getMaxHealth();
			}
		}
		redMax = sum;
		return sum;
	}

	/**
	 * �crit les statistiques du jeu dans un fichier
	 * @throws FileNotFoundException retourne une erreur si le fichier n'existe pas
	 */
	public void writeGame() throws FileNotFoundException {
		PrintWriter write = new PrintWriter("save.txt");
		for (GamePiece p : characters) {
			write.write(Integer.toString(p.getXP()) + "\n");
		}
		write.close();
	}
	/**
	 * enregistre la partie 
	 * @throws FileNotFoundException
	 */
	public void saveGame() throws FileNotFoundException {
		
		PrintWriter write = new PrintWriter("coords.txt");
		for (GamePiece p : board) {
			if (p!=null)
			write.write(p.save()+ this.turn+"\n");
		}
		write.close();
	}
	/**
	 * lance la partie sauvegard�e
	 * @param board le tableu de jeu
	 * @throws FileNotFoundException arrete si le fichier de sauvegarde n'existe pas
	 */
	public void loadGame(GamePiece[] board) throws FileNotFoundException{
		FileReader read3 = new FileReader("coords.txt");
		Scanner sc3 = new Scanner(read3);
		list = new GamePiece[30];
		int index =0;
		while (sc3.hasNextLine()) {
			String lineArg = sc3.nextLine();
			String[] options = lineArg.split(",");
			GamePiece mage = new GamePiece(options[0],options[1],Integer.parseInt(options[2]),Integer.parseInt(options[3]),Integer.parseInt(options[4]),Integer.parseInt(options[5]),Integer.parseInt(options[6]),
					new Ability(options[7],null,Integer.parseInt(options[8]),Integer.parseInt(options[9])),options[10],Integer.parseInt(options[11]));
			list[index] = mage;
			index++;
			this.turn=Integer.parseInt(options[12]);
		}
		sc3.close();
		
		
		FileReader read4 = new FileReader("coords.txt");
		Scanner sc4 = new Scanner(read4);
		index = 0;
		while (sc4.hasNextLine()) {
			
			if (sc4.nextLine() !=  null && board[index]!=null) {
				
			board[index].setX(list[index].getX());
			board[index].setY(list[index].getY());
			board[index].setHealth(list[index].getHealth());
			
			}
			
			index++;
		}
		sc4.close();
		



		
	}

	/**
	 * importer les pi�ces choisies
	 * @param board la table de jeu
	 * @throws FileNotFoundException retourne une erreur si le fichier n'existe pas
	 */
	public void importPieces(GamePiece[] board) throws FileNotFoundException {
		/**
		 * tous les types de pi�ces de jeu possibles
		 */
		GamePiece mage = new GamePiece("Mage", "Blue", 2, 0, 130, 20, 1, new Ability("Fireball", null, 1, 1),
				"images//mage.PNG", 0);
		GamePiece sorcerer = new GamePiece("Sorcerer", "Blue", 3, 0, 110, 23, 1, new Ability("Incantation", null, 1, 1),
				"images//sorcerer.PNG", 0);
		GamePiece necromancer = new GamePiece("Necromancer", "Blue", 4, 0, 100, 25, 1, new Ability("Curse", null, 1, 1),
				"images//necromancer.PNG", 0);
		GamePiece knight = new GamePiece("Knight", "Blue", 5, 0, 230, 10, 1, new Ability("Slash", null, 1, 3),
				"images//knight.PNG", 0);
		GamePiece paladin = new GamePiece("Paladin", "Blue", 6, 0, 250, 8, 1, new Ability("Shield Bash", null, 1, 3),
				"images//paladin.PNG", 0);
		GamePiece berserker = new GamePiece("Berserker", "Blue", 7, 0, 210, 13, 1, new Ability("Axe Fury", null, 1, 3),
				"images//Berserker.PNG", 0);
		GamePiece archer = new GamePiece("Archer", "Blue", 8, 0, 140, 20, 1, new Ability("Arrow", null, 1, 2),
				"images//archer.PNG", 0);
		GamePiece bowman = new GamePiece("Bowman", "Blue", 9, 0, 130, 25, 1, new Ability("Volley", null, 1, 2),
				"images//bowman.PNG", 0);
		GamePiece sniper = new GamePiece("Sniper", "Blue", 10, 0, 90, 29, 1, new Ability("Snipe", null, 1, 2),
				"images//sniper.PNG", 0);
		GamePiece lord = new GamePiece("Lord", "Blue", 5, 0, 170, 15, 1, new Ability("Glory", null, 1, 0),
				"images//lord.PNG", 0);
		GamePiece king = new GamePiece("King", "Blue", 5, 0, 180, 13, 1, new Ability("Majesty", null, 1, 0),
				"images//king.PNG", 0);
		GamePiece emperor = new GamePiece("Emperor", "Blue", 13, 0, 190, 11, 1, new Ability("Might", null, 1, 0),
				"images//emperor.PNG", 0);
		GamePiece assassin = new GamePiece("Assassin", "Blue", 14, 0, 100, 30, 1, new Ability("Knife", null, 1, 0),
				"images//assassin.PNG", 3);
		GamePiece thief = new GamePiece("Thief", "Blue", 15, 0, 90, 34, 1, new Ability("Swipe", null, 1, 0),
				"images//thief.PNG", 3);
		GamePiece shadow = new GamePiece("Shadow", "Blue", 1, 0, 80, 40, 1, new Ability("Consume", null, 1, 0),
				"images//shadow.PNG", 3);

		/**
		 * pi�ces ennemies
		 */
		GamePiece enemyMage = new GamePiece("Enemy Mage", "Red", 2, 11, 130, 20, 1, new Ability("Fireball", null, 1, 1),
				"images//enemyMage.PNG", 0);
		GamePiece enemySorcerer = new GamePiece("Enemy Sorcerer", "Red", 3, 11, 110, 23, 1,
				new Ability("Incantation", null, 1, 1), "images//enemySorcerer.PNG", 0);
		GamePiece enemyKnight = new GamePiece("Enemy Knight", "Red", 4, 11, 230, 10, 1,
				new Ability("Slash", null, 1, 3), "images//enemyKnight.PNG", 0);
		GamePiece enemyPaladin = new GamePiece("Enemy Paladin", "Red", 5, 11, 250, 8, 1,
				new Ability("Shield Bash", null, 1, 3), "images//enemyPaladin.PNG", 0);
		GamePiece enemyArcher = new GamePiece("Enemy Archer", "Red", 6, 11, 140, 20, 1,
				new Ability("Arrow", null, 1, 2), "images//enemyArcher.PNG", 0);
		GamePiece enemyBowman = new GamePiece("Enemy Bowman", "Red", 7, 11, 130, 25, 1,
				new Ability("Volley", null, 1, 2), "images//enemyBowman.PNG", 0);
		GamePiece enemyLord = new GamePiece("Enemy Lord", "Red", 8, 11, 170, 15, 1, new Ability("Glory", null, 1, 0),
				"images//enemyLord.PNG", 0);
		GamePiece enemyKing = new GamePiece("Enemy King", "Red", 9, 11, 180, 13, 1, new Ability("Majesty", null, 1, 0),
				"images//enemyKing.PNG", 0);
		GamePiece enemyAssassin = new GamePiece("Enemy Assassin", "Red", 10, 11, 100, 30, 1,
				new Ability("Knife", null, 1, 0), "images//enemyAssassin.PNG", 1);
		GamePiece enemyThief = new GamePiece("Enemy Thief", "Red", 11, 11, 90, 34, 1, new Ability("Swipe", null, 1, 0),
				"images//enemyThief.PNG", 1);

		/**
		 * d�finit les descriptions des capacit�s1 pour chaque type
		 */
		mage.updateAbilityDescription(new String[] { "Casts a fireball", "dealing " + mage.getDamage() + " damage to",
				"the target location", "\n", "Pros: Med. Range & Damage", "Cons: Med. Health" });
		sorcerer.updateAbilityDescription(
				new String[] { "Summons an arcane bolt", "dealing " + sorcerer.getDamage() + " damage to",
						"the target location", "\n", "Pros: Med. Range & Damage", "Cons: Med. Health" });
		necromancer.updateAbilityDescription(
				new String[] { "Casts dark magic", "dealing " + necromancer.getDamage() + " damage to",
						"the target location", "\n", "Pros: Med. Range & Damage", "Cons: Med. Health" });
		knight.updateAbilityDescription(
				new String[] { "Cleaves with the sword", "dealing " + knight.getDamage() + " damage to",
						"the target location", "\n", "Pros: High Health & AOE", "Cons: Low Damage" });
		paladin.updateAbilityDescription(
				new String[] { "Slams shield on ground", "dealing " + paladin.getDamage() + " damage to",
						"the target location", "\n", "Pros: High Health & AOE", "Cons: Low Damage" });
		berserker.updateAbilityDescription(
				new String[] { "Twirls double axes", "dealing " + berserker.getDamage() + " damage to",
						"the target location", "\n", "Pros: High Health & AOE", "Cons: Low Damage" });
		archer.updateAbilityDescription(new String[] { "Fires an arrow", "dealing " + archer.getDamage() + " damage to",
				"the target location", "\n", "Pros: High Range", "Cons: Low Health" });
		bowman.updateAbilityDescription(new String[] { "Fires a volley", "dealing " + bowman.getDamage() + " damage to",
				"the target location", "\n", "Pros: High Range", "Cons: Low Health" });
		sniper.updateAbilityDescription(new String[] { "Takes a shot", "dealing " + sniper.getDamage() + " damage to",
				"the target location", "\n", "Pros: High Range", "Cons: Low Health" });
		lord.updateAbilityDescription(
				new String[] { "Calls a rain of glory", "dealing " + lord.getDamage() + " damage to",
						"the target location", "\n", "Pros: Med. Damage & AOE", "Cons: Med. Health" });
		king.updateAbilityDescription(
				new String[] { "Strikes down his foes", "dealing " + king.getDamage() + " damage to",
						"the target location", "\n", "Pros: Med. Damage & AOE", "Cons: Med. Health" });
		emperor.updateAbilityDescription(
				new String[] { "Rends the earth nearby", "dealing " + emperor.getDamage() + " damage to",
						"the target location", "\n", "Pros: Med. Damage & AOE", "Cons: Med. Health" });
		assassin.updateAbilityDescription(
				new String[] { "Stabs with a knife", "dealing " + assassin.getDamage() + " damage to",
						"the target location", "\n", "Pros: High Damage", "Cons: Low Health" });
		thief.updateAbilityDescription(new String[] { "Lashes out", "dealing " + thief.getDamage() + " damage to",
				"the target location", "\n", "Pros: High Damage", "Cons: Low Health" });
		shadow.updateAbilityDescription(new String[] { "Takes a bite", "dealing " + shadow.getDamage() + " damage to",
				"the target location", "\n", "Pros: High Damage", "Cons: Low Health" });

		enemyMage.updateAbilityDescription(
				new String[] { "Casts a fireball", "dealing " + enemyMage.getDamage() + " damage to",
						"the target location", "\n", "Pros: Med. Range & Damage", "Cons: Med. Health" });
		enemySorcerer.updateAbilityDescription(
				new String[] { "Summons an arcane bolt", "dealing " + enemySorcerer.getDamage() + " damage to",
						"the target location", "\n", "Pros: Med. Range & Damage", "Cons: Med. Health" });
		enemyKnight.updateAbilityDescription(
				new String[] { "Cleaves with the sword", "dealing " + enemyKnight.getDamage() + " damage to",
						"the target location", "\n", "Pros: High Health & AOE", "Cons: Low Damage" });
		enemyPaladin.updateAbilityDescription(
				new String[] { "Slams shield on ground", "dealing " + enemyPaladin.getDamage() + " damage to",
						"the target location", "\n", "Pros: High Health & AOE", "Cons: Low Damage" });
		enemyArcher.updateAbilityDescription(
				new String[] { "Fires an arrow", "dealing " + enemyArcher.getDamage() + " damage to",
						"the target location", "\n", "Pros: High Range", "Cons: Low Health" });
		enemyBowman.updateAbilityDescription(
				new String[] { "Fires a volley", "dealing " + enemyBowman.getDamage() + " damage to",
						"the target location", "\n", "Pros: High Range", "Cons: Low Health" });
		enemyLord.updateAbilityDescription(
				new String[] { "Calls a rain of glory", "dealing " + enemyLord.getDamage() + " damage to",
						"the target location", "\n", "Pros: Med. Damage & AOE", "Cons: Med. Health" });
		enemyKing.updateAbilityDescription(
				new String[] { "Strikes down his foes", "dealing " + enemyKing.getDamage() + " damage to",
						"the target location", "\n", "Pros: Med. Damage & AOE", "Cons: Med. Health" });
		enemyAssassin.updateAbilityDescription(
				new String[] { "Stabs with a knife", "dealing " + enemyAssassin.getDamage() + " damage to",
						"the target location", "\n", "Pros: High Damage", "Cons: Low Health" });
		enemyThief.updateAbilityDescription(
				new String[] { "Lashes out", "dealing " + enemyThief.getDamage() + " damage to", "the target location",
						"\n", "Pros: High Damage", "Cons: Low Health" });

		GamePiece[] options = { mage, sorcerer, necromancer, archer, bowman, sniper, knight, paladin, berserker,
				assassin, thief, shadow, lord, king, emperor, enemyMage, enemySorcerer, enemyKnight, enemyPaladin,
				enemyArcher, enemyBowman, enemyAssassin, enemyThief, enemyLord, enemyKing };
		
		characters = options;

		/**
		 * imports the 10 pieces
		 */
		FileReader read1 = new FileReader("importPieces.txt");
		Scanner sc1 = new Scanner(read1);
		int index = 0;
		while (sc1.hasNextLine()) {
			String lineArg = sc1.nextLine();
			
			board[index] = options[Integer.parseInt(lineArg)];
			options[Integer.parseInt(lineArg)].select(characters[index], characters, this, board);
			index++;
		}
		sc1.close();

		/**
		 * imports the xp of all the pieces
		 */
		FileReader read2 = new FileReader("save.txt");
		Scanner sc2 = new Scanner(read2);
		index = 0;
		while (sc2.hasNextLine()) {
			String lineArg = sc2.nextLine();
			options[index].setXP(Integer.parseInt(lineArg));
			index++;
		}
		sc2.close();

		int boardLocationBlue = 3;
		int boardLocationRed = 12;
		/**
		 * d�finit les coordonn�es xy de toutes les pi�ces
		 */
		for (GamePiece p : board) {
			if (p!=null) {
				if (p.getTeamAsInt() == 0) {
				p.setY(0);
				p.setX(boardLocationBlue);
				boardLocationBlue++;
			} else {
				p.setY(11);
				p.setX(boardLocationRed);
				boardLocationRed--;
			}
			}
			
		}
	}

	/**
	 * d�finit la pi�ce marqu� comme �tant celui aux coordonn�es pass�es et le retourne
	 * @param x coordonn� X de la table (pixel)
	 * @param y coordonn� X de la table (pixel)
	 * @return la pi�ce marqu�
	 */
	public GamePiece markPiece(int x, int y) {
		markedPiece = getPiece(x, y);
		return markedPiece;
	}

	/**
	 * retourne la pi�ce aux coordonn�es
	 * @param x coordonn� X de la table (pixel)
	 * @param y coordonn� Y de la table (pixel)
	 * @return une pi�ce 
	 */
	public GamePiece getPiece(int x, int y) {
		for (GamePiece p : board) {
			if (p != null && p.isAlive() && p.getX() == ((x - 25) / 75) && p.getY() == ((y - 25) / 79)) {
				return p;
			}
		}
		return null;
	}

	/**
	 * d�place la pi�ce marqu�e aux coordonn�es pass�es
	 * @param x coordonn� X de la table (pixel)
	 * @param y coordonn� Y de la table (pixel)
	 */
	public void movePiece(int x, int y) {
		if (this.markedPiece != null) {
			int xOld = markedPiece.getX();
			int yOld = markedPiece.getY();
			if (markedPiece != null && getPiece(x, y) == null) {
				markedPiece.setX(((x - 25) / 75));
				markedPiece.setY(((y - 25) / 79));
				updateMoves(xOld, yOld, markedPiece, 0);
				markedPiece = null;
			}

		}
	}

	/**
	 * handles all the different attack types and calls attackCoords for each location targeted
	 * @param x la coordonn�e X de la victime
	 * @param y la coordonn�e Y de la victime
	 * @param attackerX la coordonn�e X de l'attaquant
	 * @param attackerY la coordonn�e Y de l'attaquant
	 */
	public void attackPiece(int x, int y, int attackerX, int attackerY) {
		// single spot abilities
		Random random = new Random();
		if (this.markedPiece != null && (markedPiece.getAbilityType() == 0 || markedPiece.getAbilityType() == 1
				|| markedPiece.getAbilityType() == 2)) {
			for (int i = 0; i < 10; i++) {

				boolean takeIt = random.nextBoolean();
				if (takeIt) {
					attackCoords(x, y, attackerX, attackerY);
				} else {
					attackCoords(attackerX, attackerY, x, y);
				}

			}
			update();
		}
		/**
		 * Melee Aoe tout autour de l'emplacement central de la pi�ce marqu�e.
		 */
		else if (this.markedPiece != null && markedPiece.getAbilityType() == 3) {

			for (int k = 0; k < 10; k++) {
				boolean takeIt = random.nextBoolean();
				for (int i = -1; i <= 1; ++i) {
					for (int j = -1; j <= 1; ++j) {
						if ((markedPiece.getX() * 75) + 25 + 75 * i >= 0
								&& (markedPiece.getX() * 75) + 25 + 75 * i <= 1225
								&& (markedPiece.getY() * 79) + 25 + 79 * j >= 0
								&& (markedPiece.getY() * 79) + 25 + 79 * j <= 974) {
							x = (markedPiece.getX() * 75) + 25 + 75 * i;
							y = (markedPiece.getY() * 79) + 25 + 79 * j;
							if (takeIt) {
								attackCoords(x, y, attackerX, attackerY);
							} else {
								attackCoords(attackerX, attackerY, x, y);
							}
						}
					}
				}
			}
			update();
		}
		markedPiece = null;

	}

	/**
	 * attaque la pi�ce aux coordonn�es de la pi�ce marqu�e
	 * @param x la coordonn�e X de la victime
	 * @param y la coordonn�e Y de la victime
	 * @param attackerX la coordonn�e X de  l'attaquant
	 * @param attackerY la coordonn�e Y de l'attaquant
	 */
	public void attackCoords(int x, int y, int attackerX, int attackerY) {
		if (getPiece(attackerX, attackerY) != null && getPiece(x, y) != null
				&& (!getPiece(attackerX, attackerY).getTeam().equals(getPiece(x, y).getTeam())
						|| !getPiece(attackerX, attackerY).equals(getPiece(x, y)))) {
			int dmg = 0;
			updateMoves(x, y, getPiece(attackerX, attackerY), 2);
			if (getPiece(x, y).getTeam().equals("Blue")) {
				dmg = getPiece(attackerX, attackerY).getDamage();
				blueDmg += dmg;
			} else {
				dmg = getPiece(attackerX, attackerY).getDamage();
				redDmg += dmg;
			}
			GamePiece curr = getPiece(x, y);
			curr.setHealth(curr.getHealth() - dmg);

		}
	}

	/**
	 * renvoie 0 pour le bleu et 1 pour le rouge
	 * @return le tour de qui
	 */
	public int getTurn() {
		return turn;
	}

	/**
	 * returns true if the two passed pieces are different colors, false if they are the same color
	 * @param piece1 la premi�re pi�ce 
	 * @param piece2 la deuxi�me pi�ce
	 * @return boolean
	 */
	public boolean isValidAttack(GamePiece piece1, GamePiece piece2) {
		if (piece1 == null || piece2 == null) {
			return false;
		}
		if (!piece1.getTeam().equals(piece2.getTeam())) {
			/**
			 * port�e ou m�l�e de 1 ou aoe m�l�e de 1
			 */
			if ((piece1.getAbilityType() == 0 || piece1.getAbilityType() == 3)
					&& Math.abs(piece1.getX() - piece2.getX()) <= 1 && Math.abs(piece1.getY() - piece2.getY()) <= 1) {
				return true;
			}
			/**
			 * ranged of range 2
			 */
			else if (piece1.getAbilityType() == 1 && Math.abs(piece1.getX() - piece2.getX()) <= 2
					&& Math.abs(piece1.getY() - piece2.getY()) <= 2) {
				return true;
			}
			/**
			 * ranged of range 3
			 */
			else if (piece1.getAbilityType() == 2 && Math.abs(piece1.getX() - piece2.getX()) <= 3
					&& Math.abs(piece1.getY() - piece2.getY()) <= 3) {
				return true;
			}
			return false;
		}
		return false;
	}

	/**
	 * cette m�thode met ou non une pi�ce dans la partie
	 * @param i
	 * @param j
	 * @param characters la liste des guerriers
	 */
	public void select(int i, int j, GamePiece[] characters) {
		int index = 15 * j + (int) Math.round((0.178479 + 0.759665 * i));
		if (index < characters.length) {
			if (characters[index].isSelected()) {
				characters[index].deselect(characters[index], characters, this, board);
			} else {
				characters[index].select(characters[index], characters, this, board);
			}
		}
	}
	

	
	

	/**
	 * returns true if the x,y location is a valid move for the given piece considering the spot is empty already
	 * @param x
	 * @param y
	 * @param piece
	 * @return valide ou pas
	 */
	public boolean isValidMove(int x, int y, GamePiece piece) {
		x = (x - 25) / 75;
		y = (y - 25) / 79;
		if (piece != null) {
			// default moveType of 1 square in any direction
			if (piece.getMoveType() == 0 && Math.abs(piece.getX() - x) <= 3 && Math.abs(piece.getY() - y) <= 3) {
				return true;
			}
			// movetype of 2 squares in any direction// jumps over other pieces
			else if (piece.getMoveType() == 1 && Math.abs(piece.getX() - x) <= 4 && Math.abs(piece.getY() - y) <= 4) {
				return true;
			}

			// movetype of 3 squares in any direction// jumps over other pieces
			else if (piece.getMoveType() == 3
					&& (y == piece.getY() && x != piece.getX() || y != piece.getY() && x == piece.getX())
					&& Math.abs(piece.getX() - x) <= 5 && Math.abs(piece.getY() - y) <= 5) {
				return true;
			}
			return false;
		}
		return false;
	}
}